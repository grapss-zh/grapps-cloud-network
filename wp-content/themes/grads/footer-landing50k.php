




<footer>

  <section class="contact-details">

    <img class="triangleTop" src="<?php echo get_bloginfo('template_directory');?>/img/landing50k/tri-white-top.png" alt="">
    <a tabindex="33" class="grapps-logo" href="http://grapps.io">
      <img src="<?php echo get_bloginfo('template_directory');?>/img/grapps.png"  alt="GRapps - Focus on you - Company Logo" />
    </a>
    <div class="separator80"></div>


    <section id="footerContainer" class="section-160-30 ">

      <a tabindex="34" target="_blank" href="https://twitter.com/GRappsIO"><i class="fa fa-twitter"></i></a>
      <a tabindex="35" target="_blank" href="https://www.facebook.com/grdevelo/"><i class="fa fa-facebook"></i></a>
      <a tabindex="36" target="_blank" href="https://plus.google.com/105513543485231554933/videos"><i class="fa fa-google-plus"></i></a>

      <div class="separator80"></div>
      <p>Copyright © 2016<br>Design by <a tabindex="37" href="http://grapps.io">GRapps - Mobile & Web Development</a></p>
    </section>


  </section>

</footer>
<?php do_shortcode('[tawkto]'); ?>
</body>
</html>
