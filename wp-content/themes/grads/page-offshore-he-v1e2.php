
<?php
/**
 * Template Name: Offshore he v1e2
 *
 * @package WordPress
 * @subpackage GRapps
 * @since GRapps 1.0
 */
?>

<?php
//TODO: DELETE
// , , ,
?>


<?php get_header('offshore-he-v1'); ?>
<?php $grapps_lang = 'he'; ?>

<div id="ng-app" ng-app="offshore-he-v1" class="page-wrap grlanding50kb">
<?php
$topBgImage = get_field('top_background_image');
if( !empty($topBgImage) ) { ?>
    <section id="top-container" style="background-image: url('<?= $topBgImage['url']; ?>')">
<?php } else { ?>
    <section id="top-container">
<?php } ?>


        <a class="grapps-logo" href="#">
            <img tabindex="1" src="<?php echo get_bloginfo('template_directory');?>/img/grappswhite.png"  alt="GRapps - Focus on you - Company Logo" />
        </a>

        <div class="container-fluid">
            <div class="row top-row">

                <div class="col-sm-12 col-md-7 col-md-push-5 no-gutter">
                    <div class="sliderText">


                        <p class="ls-l ">
                            <br class="hidden-xs">
                            <span tabindex="3"  class="second-line">
                                איך לגייס מפתח איכותי ולחסוך בזמן ועלויות ללא סיכונים וללא התחייבות
                            </span>


                            <div class="slider-p-text">
                        <p tabindex="5" >שוק הסטארטאפים הישראלי דוהר קדימה בקצב מסחרר, אלפי חברות נפתחות בארץ בכל שנה וחברות בינלאומיות רבות מקימות מרכזי פיתוח בישראל. אך לצד ההתפתחות המהירה והכסף הרב שמושקע בענף, בכל הקשור לכוח אדם - אנחנו פשוט  לא עומדים בקצב.
                        </p>
                        <p class="hidden-xs">
                            חברים, הנתונים מדברים בעד עצמם, בישראל של 2017 קיים מחסור חמור של מהנדסים ומתכנתים. על-פי הערכת רשות החדשנות (לשעבר המדען הראשי) במשרד הכלכלה והתעשייה, המחסור יגיע ל-10,000 מהנדסים ומתכנתים בתוך כעשור בלבד!!!
                            כבר היום אנחנו צופים במחסור של 4500 מפתחים בתעשייה ובעשור הקרוב מספר זה עתיד להכפיל את עצמו…
                        </p>
                        <p>
                            נכון שלא באמת הייתם צריכים את ההקדמה הזאת בשביל להבין את גודל הבעיה?
                        </p>
                        <p style="font-weight: bold">
                            הפתרון שלנו הוא גיוס מפתחים משלכם במודל Offshore באוקראינה
                        </p>
                        <p style="font-weight: bold" >
                            ההבטחה שלנו אליכם היא פשוטה, נקבל מכם דרישות משרה ותוך שבועיים לכל היותר נקבע לכם ראיון ראשון עם מועמד רלוונטי וכל זה ללא עלות!!!
                        </p>
                        </div>

                        <div class="button-wrap">
                            <a tabindex="6" onclick="return facebookEventClickedOnLearnMore();" href="#aboutContainer" class="buttonBig">למד עוד</a>
                            <a tabindex="7" onclick="return facebookEventClickedOnContactUsNow();" href="#contactUs" class="buttonBig">צור קשר</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-5 col-md-pull-7 top-image-wrap">
                    <img class="top-img-slider" src="<?php echo get_bloginfo('template_directory');?>/img/offshore-he/paper2.png" alt="Phone" class="ls-l ls-preloaded">
                </div>
        </div>


        </div>
    </section>



<section id="aboutContainer" class="section-80-130 whiteBgSection">

    <img class="triangleTop" src="<?php echo get_bloginfo('template_directory');?>/img/offshore-he/tri-white-top.png" alt="">


    <div class="about-grapps">
        <h4 class="focus-test">FOCUS ON YOU</h4>
        <div class="container">
            <div class="row">

                <div class="col-sm-6 col-md-7 col-sm-push-6 col-md-push-5">
                    <div class="about-content">
                        <h2 class="title" tabindex="16">בדיקת התאמה</h2>
                        אם אתם יזמים, בעלי חברת סטארטאפ בישראל של 2017, תנסו לענות על השאלות הבאות בעצמכם

                        <ul>
                            <li>אתם מוצאים את עצמיכם משקיעים זמן יקר בגיוס מפתחים?</li>
                            <li>מגיעים אליכם הרבה מאוד קורות חיים, אבל רק לאחוז קטן מהאנשים (אם בכלל) יש את הניסיון הרלוונטי הדרוש לכם?</li>
                            <li>במהלך החודש האחרון העליתם לרשתות החברתיות השונות 10 פוסטים לפחות המכילים את צמד המילים We're Hiring ?</li>
                            <li>מצאתם את עצמיכם מנהלים משאים ומתנים קשים עם מועמדים בעקבות דרישות של טאלנטים מחוזרים במיוחד, כאלה שענו בדיוק על דרישות המשרה שלכם?</li>
                        </ul>

                        <p>
                            אם עניתם ״כן״ על אחת מהשאלות הבאות, תעזבו הכל וצרו איתנו קשר!
                            <i class="fa fa-arrow-left bounce hidden-xs"></i><i class="fa fa-arrow-down visible-xs contact2-arrow-down "></i>
                        </p>

                        <p class="promise">

                            ההבטחה שלנו אליכם היא פשוטה, נקבל מכם דרישות משרה ותוך שבועיים
                            <strong>
                                לכל היותר
                            </strong>
                            נקבע לכם ראיון ראשון עם מועמד רלוונטי
                            <span class="underline">וכל זה ללא עלות!!!</span>
                        </p>
                    </div>
                </div>



                <div class="col-sm-6 col-md-5 col-sm-pull-6 col-md-pull-7">
                    <div class="contactUs2"  ng-controller="Contact2Controller">
                        <form name="userForm" novalidate>
                            <h2 class="get-offer-now-title">השאירו פרטים כאן ואחד מנציגינו יחזרו אליכם בהקדם!</h2>
                            <input type="hidden" name="action" value="contact_send" />
                            <div class="form-group" show-errors='{showSuccess: true}'>
                                <label class="control-label">* <?php _e('שם', 'grapps'); ?></label>
                                <input tabindex="9" type="text" class="form-control" name="name" ng-model="user.name" required placeholder="<?php _e('מה השם שלך?','grapps');?>" />
                                <p tabindex="8" class="help-block ng-class:{ 'details-err-msg': userForm.name.$error.required }" ng-if="userForm.name.$error.required"><?php _e('אנא הכנס את שמך','grapps'); ?></p>
                            </div>




                            <div class="form-group email-wrap input-field " show-errors='{showSuccess: true}'>
                                <label class="control-label">* <?php _e('אימייל','grapps'); ?></label>
                                <input tabindex="12" type="email" class="form-control" name="email" ng-model="user.email" required placeholder="<?php _e('כתובת המייל שלך','grapps'); ?>" />

                                <p tabindex="11" class="help-block ng-class:{ 'details-err-msg': userForm.email.$error.required }" ng-if="userForm.email.$error.required"><?php _e('אנא הכנס כתובת מייל','grapps');?></p>
                                <p tabindex="10" class="help-block ng-class:{ 'details-err-msg': userForm.email.$error.email }" ng-if="userForm.email.$error.email"><?php _e('כתובת מייל לא תקינה','grapps');?></p>
                            </div>




                            <div class="form-group phone-wrap input-field " show-errors='{showSuccess: true}'>
                                <label class="control-label">טלפון</label>
                                <input tabindex="13" type="phone" class="form-control" name="phone" ng-model="user.phone" required placeholder="<?php _e('מספר הטלפון שלך','grapps'); ?>" />

                            </div>





                            <div class="btn-contact-wrap">
                                <span ng-show="isResponse" class="form-response-message" ng-class="responseError == true ? 'respose-fail' : '' ">{{user.thankyouorfailed}}</span>
                                <span ng-show="isLoading" class="loading-spinner">
                  <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                  <span class="sr-only">Loading...</span>
                  </span>
                                <button tabindex="14" ng-click="save()" type="submit" value="Send Message" class="btn btn-lg btn-success submit"  aria-label="שלח" ><?php _e('שלח','grapps'); ?></button>
                            </div>
                        </form>
                    </div>
                </div>


            </div>
        </div>
        <img class="triangleBottom" src="<?php echo get_bloginfo('template_directory');?>/img/offshore-he/tri-white-bot.png" alt="">

    </div>

<?php
$OurStoryBgImage = get_field('our_story_background_image');
if( !empty($OurStoryBgImage) ) { ?>
    <section class="contact-title the-how" style="background-image: url('<?= $OurStoryBgImage['url']; ?>'); background-size: 100% 100% ">
<?php } else { ?>
    <section class="contact-title the-how">
    <?php } ?>
        <div class="the-how-wrap">
            <h2 class="title">הסיפור שלנו</h2>
            <p>
                העניין הוא שאנחנו, כחברה טכנולוגית שעוסקת בפיתוח אפליקציות, חווינו את הקושי הזה על בשרינו, כמות המשאבים (בעיקר זמן וכסף) שהשקענו בתחילת דרכינו בשביל לגייס מפתח איכותי לחברה היו עצומים, אבל חמור מזה, לעיתים אף נאלצנו להתפשר בדרישות ולהוריד סטנדרטים נוכח ההיצע הנמוך ומורכבות תהליך הפיתוח וגיוס מפתחים.
            </p>
            <p>
                אנחנו משוכנעים שאתם יודעים זאת טוב לפחות כמונו, תהליך הגיוס הצריך מאיתנו להשקיע זמן בפרסום המשרה עבור מועמדים רלוונטיים (במקרים מסויימים לקחו כ 3-4 חודשים עד שהגיע מועמד באמת רלוונטי), להשקיע זמן במעבר על קורות חיים (היה שלב שחשבנו לגייס מנהלת HR בשביל זה), להשקיע זמן בראיונות טלפונים, להשקיע זמן בעריכת מבחנים מקצועיים ובדיקתם, להשקיע פעם נוספת זמן בעריכת ראיונות נוספים למועמדים המובילים ואז החלק ה״כייפי״... משא ומתן על דרישות השכר....
                ובמידה והכל הלך חלק, תוך 3-6 חודשים הצלחנו לגייס את הטאלנט שלנו!

            </p>
            <p>
                אז זה הסיפור שלנו, כמו בכל תחום בחיים, לא הסכמנו לקבל את המציאות כפי שהיא, הבנו שחייב להיות פתרון שנותן מענה לעניין והבנו שברגע שנמצא אותו פתרון, נבנה את המודל הנכון והמדויק שיאפשר לנו להציע את הפתרון לחברות אחרות שחוות את אותה הבעיה כמונו.
                וכך, לפני קצת פחות משנה ולאחר מחקר שוק מטורף, הקמנו סניף ראשון של GRapps באוקראינה!
            </p>
            <p>
                היום אנחנו גאים להעסיק כ 25 מפתחים ומהנדסים (and still counting ) בשני סניפים באוקראינה, אחד בזי׳טומיר והשני בקייב אשר משרתים כ 10 (and still counting ) חברות סטארטאפ ישראליות במודל שנקרא על פי רוב Offshore service .
            </p>
        </div>








	    <?php
	    $images = get_field('our_story_gallery_slider');
	    if( $images ) { ?>
            <div class="slick-office-wrap" style="direction: ltr">
                <div id="slick-office">
	                <?php foreach( $images as $image ) { ?>
                        <div class="item">
                            <a href="<?php echo $image['url']; ?>" data-rel="lightcase:office">
                                <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>">
                            </a>
                        </div>
	                <?php } ?>
	                <?php foreach( $images as $image ) { ?>
                        <div class="item">
                            <a href="<?php echo $image['url']; ?>" data-rel="lightcase:office">
                                <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>">
                            </a>
                        </div>
	                <?php } ?>
                </div>
            </div>
	    <?php } ?>



   </section>

    <section class="contact-title the-process">
        <img class="triangleTop" src="<?php echo get_bloginfo('template_directory');?>/img/offshore-he/tri-white-top.png" alt="">
        <div class="process-bubbles">
            <h2 class="title">אז איך הקמת המשרדים שלנו באוקראינה תפתור לכם את בעיית הגיוס?</h2>
            <h3 class="subtitle">באף מקום בעולם לא קל למצוא אנשי מקצוע מעולים, אך כמו ברוב המקרים, הנסיון משחק תפקיד מרכזי. ההיכרות שלנו את השוק המקומי והידע שצברנו עם השנים, מאפשרים לנו לספק ללקוחות שלנו גישה למהנדסים ולמפתחים מעולים בעלויות הרבה יותר נמוכות מהשוק הישראלי. </h3>
            <div class="container">

                <div class="row">

                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="iconColWrap">
                            <i class="fa fa-file-text"></i>
                            <h3>חוזים פשוטים</h3>
                            <p>אין כל יחסי עובד מעביד!! אתם בפועל עובדים מול מפתח שלכם בצוות שהקמתם אבל מתנהלים חשבונאית מול חברה ישראלית כנגד חשבונית מס!
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="iconColWrap">
                            <i class="fa fa-heart"></i>
                            <h3>אפס סיכון</h3>
                            <p>בכל תהליך האיתור והמיון אין שום סיכון שלכם! אין אותיות קטנות, אין הפתעות, פשוט אפס סיכון מבחינתכם.</p>
                        </div>
                    </div>
                    <div class="clearfix visible-sm"></div>

                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="iconColWrap">
                            <i class="fa fa-filter"></i>
                            <h3>תהליך המיון עלינו</h3>
                            <p>תהליך הגיוס מתנהל בדיוק כמו בארץ רק שאצלנו  אתם לא צריכים להשקיע בכך משאבים. אנחנו מנהלים תהליך מיון קפדני כאשר בסופו המועמדים המובילים עוברים לאישורכם הסופי
                            </p>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="iconColWrap">
                            <i class="fa fa-usd"></i>
                            <h3>עלויות נמוכות</h3>
                            <p>ההוצאות החודשיות שלכם עבור החזקת  המפתח נמוכות משמעותית מאוד לעומת החזקת מפתח ברמה זהה בישראל, חיסכון של כ 60% ואף יותר!!!</p>
                        </div>
                    </div>




                </div>
                <br>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="iconColWrap">
                            <i class="fa fa-users"></i>
                            <h3>היצע מפתחים</h3>
                            <p>יש לנו נגישות להיצע עצום של מפתחים בעלי נסיון עשיר ורלוונטי במגוון תחומי פיתוח. </p>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="iconColWrap">
                            <i class="fa fa-clock-o"></i>
                            <h3>אזור זמן</h3>
                            <p>אזור הזמן באוקראינה הוא אותו time zone בדיוק כמו בישראל (אפילו בשעון חורף :))</p>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="iconColWrap">
                            <i class="fa fa-language"></i>
                            <h3>שפה משותפת</h3>
                            <p>דוברי אנגלית וכמובן שרוסית (שזה אחלה עבור סטארטאפים שמעסיקים דוברי רוסית בארץ)</p>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="iconColWrap">
                            <i class="fa fa-plane"></i>
                            <h3>טיסות</h3>
                            <p>בסה״כ 3 שעות טיסה ובמחירי טיסה נוחים ביותר - במידה ותרצו להיפגש למספר ימים עם הצוות שלכם!</p>
                        </div>
                    </div>



                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>
</section>








<?php
$FormBgImage = get_field('form_background_image');
if( !empty($FormBgImage) ) { ?>
<section id="contactUs" class="contact-us"
         ng-controller="ContactController"
         style="background-image: url('<?= $FormBgImage['url']; ?>')">

<?php } else { ?>
<section id="contactUs" class="contact-us"   ng-controller="ContactController">
<?php } ?>

    <img class="triangleBottom" src="<?php echo get_bloginfo('template_directory');?>/img/offshore-he/tri-white-bot.png" alt="">


    <div class="container">
        <div class="row">

            <h2 class="contact-title">
                <h2 class="main-title">הצטרפו לחברות שכבר עשו את הצעד</h2>
                <h3 style="text-align: center" class="subtitle">ברגע זה החברות האלו באמצע תהליכי פיתוח</h3>

	            <?php
	            $images = get_field('clients_gallery_slider');
	            if( $images ) { ?>
                    <div class="slick-clients-wrap" style="direction: ltr">
                        <div id="slick-clients">
				            <?php foreach( $images as $image ) { ?>
                                <div class="item">
                                        <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>">
                                </div>
				            <?php } ?>
				            <?php foreach( $images as $image ) { ?>
                                <div class="item">
                                    <a href="<?php echo $image['url']; ?>">
                                        <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>">
                                    </a>
                                </div>
				            <?php } ?>
                        </div>
                    </div>
	            <?php } ?>


                <h2 class="main-title-2">הצטרפו עכשיו</h2>
            </div>


            <div class="col-sm-6 col-sm-push-6">
                <div id="contact-form" class="form">
                    <h3>השאירו פרטים כאן ואחד מנציגינו יחזרו אליכם בהקדם</h3>


                    <form name="userForm" novalidate>
                        <input type="hidden" name="action" value="contact_send" />
                        <div class="form-group" show-errors='{showSuccess: true}'>
                            <label class="control-label"><?php _e('שם', 'grapps'); ?></label>
                            <input tabindex="19" type="text" class="form-control" name="name" ng-model="user.name" required placeholder="<?php _e('השם שלך','grapps');?>" />
                            <p tabindex="18" class="help-block ng-class:{ 'details-err-msg': userForm.name.$error.required }" ng-if="userForm.name.$error.required"><?php _e('Your name is required','grapps'); ?></p>
                        </div>

                        <div class="form-group heightFix" show-errors='{showSuccess: true}'>


                            <label class="control-label"><?php _e('נושא','grapps'); ?></label>
					        <?php if($grapps_lang == 'he') { ?>
                            <select tabindex="24" name="subject" ng-model="user.subject" class="form-control input-subject" required  title="Subject" aria-label="Subject" ng-options="subject.subject as subject.subject for subject in subjectListHeb">
						        <?php } else { ?>
                                <select tabindex="24" name="subject" ng-model="user.subject" class="form-control input-subject" required  title="Subject" aria-label="Subject" ng-options="subject.subject as subject.subject for subject in subjectList">

							        <?php } ?>
                                </select>
                        </div>




                        <div class="form-group email-wrap input-field " show-errors='{showSuccess: true}'>
                            <label class="control-label"><?php _e('אימייל','grapps'); ?></label>
                            <input tabindex="28" type="email" class="form-control" name="email" ng-model="user.email" required placeholder="<?php _e('כתובת המייל שלך','grapps'); ?>" />

                            <p tabindex="26" class="help-block ng-class:{ 'details-err-msg': userForm.email.$error.required }" ng-if="userForm.email.$error.required"><?php _e('אנא הכנס כתובת מייל','grapps');?></p>
                            <p tabindex="27" class="help-block ng-class:{ 'details-err-msg': userForm.email.$error.email }" ng-if="userForm.email.$error.email"><?php _e('כתובת מייל לא תקינה','grapps');?></p>
                        </div>




                        <div class="form-group content-wrap input-field ">
                            <label class="control-label"><?php _e('הודעה','grapps') ;?></label>
                            <textarea tabindex="28" name="message" id="content" class="form-control" rows="5" ng-model="user.content"></textarea>
                        </div>

                        <div class="btn-contact-wrap">
                            <span ng-show="isResponse" class="form-response-message" ng-class="responseError == true ? 'respose-fail' : '' ">{{user.thankyouorfailed}}</span>
                            <span ng-show="isLoading" class="loading-spinner">
								<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
								<span class="sr-only">טוען...</span>
								</span>
                            <button tabindex="29" ng-click="save()" type="submit" value="Send Message" class="btn btn-lg btn-success submit"  aria-label="Sendי" ><?php _e('שלח','grapps'); ?></button>
                        </div>
                    </form>

                </div><!-- form -->
            </div><!-- col -->

            <div class="col-sm-6 col-sm-pull-6 preview-wrap">
                <h2 class="preview-title"><?php _e('ראה כיצד ההודעה שלך נראית בדרך אלינו','grapps'); ?></h2>

                <div class="preview">
                    <h3><?php _e('Dear GRapps','grapps');?>,</h3>
                    <h4>{{user.subject}}</h4>
                    {{user.phoneNumber}}
                    <div id="letter-content">
                        {{user.content}}
                    </div>
                    <div class="letter-sig">
                        <div><?php _e('תודה','grapps'); ?>,</div>
                        <div ng-if="!user.name" class="placeholder-email"><?php _e('ישראל ישראלי','grapps');?></div>
                        <div class="">{{user.name}}</div>
                        <div ng-if="!user.email" class="placeholder-email">info@grapps.io</div>
                        <div class="">{{user.email}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>






<section id="about2Container" class="section-80-130 whiteBgSection">

    <img class="triangleTop" src="<?php echo get_bloginfo('template_directory');?>/img/offshore-he/tri-white-top.png" alt="">


    <div class="about-grapps">
        <h4 class="focus-test-2">FOCUS ON YOU</h4>
        <div class="container">
            <div class="row">




                <div class="col-sm-7 col-sm-push-5 col-md-8 col-md-push-4">
                    <div class="about-content">
                        <h2 class="title">אודות GRapps</h2>
                        <p>
                            Grapps היא חברת פיתוח אפליקציות מובייל ופיתוח אתרים, נוסדה ב-2013.
                            אנחנו מנוסים בפיתוח אפליקציות מובייל ובניית אתרים עם אהבה גדולה למטודולוגיית Lean Startup.
                            הפילוסופיה של GRapps בנוגע לשירות לקוחות היא לא רק לבצע עבודה איכותית במחיר הוגן, זה הסטנדרט שלנו. שירות לקוחות מעולה מבחינתנו מתבטא בללוות את הלקוח ולהדריך אותו בכל שלב בדרך, להבין את הצרכים שלו ולגרום לו להשיג את המטרות שהציב לעצמו, אבל הכי חשוב מבחינתנו היא להפוך את התהליך עבור הלקוח לתהליך נעים וכיפי שיגרום לו לחייך לאורך כל הדרך.
                            כחברת פיתוח אפליקציות מנוסה שמשרתת יזמים בתהליך פיתוח המוצר, סטארטפים, חברות מדיה וחברות גדולות בתחומים שונים בתעשייה, אנחנו מוודאים שצוות הפיתוח יכסה כל צורך אפשרי ויעזור ללקוח להגיע לתוצאות משמעותיות.
                        </p>

                    </div>
                </div>

                <div class="col-sm-5 col-sm-pull-7 col-md-4 col-md-pull-8">
                    <div class="about-typo">
                        <span class="typo-tech">TECHNOLOGY</span>
                        <span class="typo-focus">FOCUS ON YOU</span>
                        <span class="typo-sidecode"><strong>GR</strong>APPS</span>
                        <span class="typo-creative">CREATIVE</span>
                        <span class="typo-bus">YOUR BUSINESS</span>

                    </div>
                </div>



            </div>
        </div>
        <img class="triangleBottom" src="<?php echo get_bloginfo('template_directory');?>/img/offshore-he/tri-white-bot.png" alt="">

    </div>


</section>


<?php
$footerBgImage = get_field('footer_strip_background_image');
if( !empty($footerBgImage) ) { ?>
    <section id="contactUsFooter" class="contact-us contact-us-footer" style="background-image: url('<?= $footerBgImage['url']; ?>')" >
<?php } else { ?>
    <section id="contactUsFooter" class="contact-us contact-us-footer" >
<?php } ?>
    <img class="triangleBottom hidden-xs" src="<?php echo get_bloginfo('template_directory');?>/img/offshore-he/tri-white-bot.png" alt="">
</section>







<?php get_footer('offshore-he-v1'); ?>
