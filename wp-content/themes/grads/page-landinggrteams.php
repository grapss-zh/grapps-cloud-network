
<?php
/**
 * Template Name: Landing GRteams
 *
 * @package WordPress
 * @subpackage GRapps
 * @since GRapps 1.0
 */
?>

<?php get_header('landinggrteams'); ?>


<div id="ng-app" ng-app="GRlanding50k" class="page-wrap grteams">

	<section id="top-container">
  <div class="bg-img"></div>
		<a class="grapps-logo grapps-logo-top " href="#">
			<img tabindex="1" src="<?php echo get_bloginfo('template_directory');?>/img/grteams/grappswhite.png"  alt="GRapps - Focus on you - Company Logo" />
		</a>
		<div class="container">
      <div class="row top-row">

        <div class="col-xs-12 col-sm-12 no-gutter">
          <div class="sliderText">


            <p class="ls-l ">
              <br>
              <div tabindex="3"  class="liner-grsprints"><span class="alpha"><img src="<?php echo get_bloginfo('template_directory');?>/img/grteams/alpha_shadow2.png" alt="Alpha greek letter"></span>Teams</div>
<!--              <div tabindex="3" class="liner-run">Run Faster!</div>-->
              <div class="second-line-wrap">
                <span tabindex="3"  class="second-line">Dedicated Developers</span><br>
                <span tabindex="3"  class="second-line">Waiting to join your team </span>
              </div>
              <div tabindex="3" class="third-line">Get the full benefits of working with remote developers,<br>
              But Still have a full-time Team that seats side by side.
              </div>
            </p>
              <!--          <br>-->
    <!--          <span class="us-market">for the US market</span>-->

<!--              <div class="slider-p-text">-->
<!--                <p tabindex="5" >Let us take care of everything, all you need to do is to guide your team to success!</p>-->
<!--                <p tabindex="4"  >GRapps Can build a Dedicated Team for you in a matter of days</p>-->
<!--            </div>-->
            <div class="button-wrap">
              <a tabindex="6" href="#aboutContainer" class="buttonBig">Learn More</a>
              <a tabindex="7" href="#contactUs" class="buttonBig">Contact Us</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>



	<section id="aboutContainer" class="section-80-130 whiteBgSection">

		<img class="triangleTop" src="<?php echo get_bloginfo('template_directory');?>/img/landing50k/tri-white-top.png" alt="">


      <div class="about-grapps">
        <h4 class="focus-test">FOCUS ON YOU</h4>
        <div class="container">
          <div class="row">
            <div class="col-sm-7 col-md-7">
              <div class="about-content">
                <h2 tabindex="15" class="title">About the <span class="alpha-2"> <img src="<?php echo get_bloginfo('template_directory');?>/img/grteams/alpha.png" alt="Alpha greek letter"></span>Teams</h2>
                <p tabindex="16" >
                  You can build your Alpha Team, with skilled developers, that are working with the most cutting edge technologies and frameworks, scale your team as needed. All of your developers will work side by side, from our Dev Center located in Europe. We will take care of everything, office equipment, contracts, administrative management and employees happiness. So you could run faster and focus on your product, your goals, and the success of your company.                </p>

              </div>
            </div>

            <div class="col-sm-5 col-md-5">
              <div class="contactUs2"  ng-controller="Contact2Controller">
                <form name="userForm" novalidate>
                  <h2 class="get-offer-now-title">Build Your<br> <img class="alpha-form" src="<?php echo get_bloginfo('template_directory');?>/img/grteams/alpha.png" alt="Alpha greek letter"><br> Team Now!</h2>
                  <input type="hidden" name="action" value="contact_send" />
                  <div class="form-group" show-errors='{showSuccess: true}'>
                    <label class="control-label">* <?php _e('Name', 'grapps'); ?></label>
                    <input tabindex="9" type="text" class="form-control" name="name" ng-model="user.name" required placeholder="<?php _e('Your name','grapps');?>" />
                    <p tabindex="8" class="help-block ng-class:{ 'details-err-msg': userForm.name.$error.required }" ng-if="userForm.name.$error.required"><?php _e('Your name is required','grapps'); ?></p>
                  </div>




                  <div class="form-group email-wrap input-field " show-errors='{showSuccess: true}'>
                    <label class="control-label">* <?php _e('Email','grapps'); ?></label>
                    <input tabindex="12" type="email" class="form-control" name="email" ng-model="user.email" required placeholder="<?php _e('Your Email Address','grapps'); ?>" />

                    <p tabindex="11" class="help-block ng-class:{ 'details-err-msg': userForm.email.$error.required }" ng-if="userForm.email.$error.required"><?php _e('Please enter your email','grapps');?></p>
                    <p tabindex="10" class="help-block ng-class:{ 'details-err-msg': userForm.email.$error.email }" ng-if="userForm.email.$error.email"><?php _e('Something is wrong with this Email address','grapps');?></p>
                  </div>




                  <div class="form-group phone-wrap input-field " show-errors='{showSuccess: true}'>
                    <label class="control-label">Phone</label>
                    <input tabindex="13" type="phone" class="form-control" name="phone" ng-model="user.phone" required placeholder="<?php _e('Your Phone Number','grapps'); ?>" />

                 </div>





                  <div class="btn-contact-wrap">
                    <span ng-show="isResponse" class="form-response-message" ng-class="responseError == true ? 'respose-fail' : '' ">{{user.thankyouorfailed}}</span>
                <span ng-show="isLoading" class="loading-spinner">
                  <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                  <span class="sr-only">Loading...</span>
                  </span>
                    <button tabindex="14" ng-click="save()" type="submit" value="Send Message" class="btn btn-lg btn-success submit"  aria-label="Sendי" ><?php _e('Send','grapps'); ?></button>
                  </div>
                </form>
              </div>
            </div>

          </div>
        </div>
        <img class="triangleBottom" src="<?php echo get_bloginfo('template_directory');?>/img/landing50k/tri-white-bot.png" alt="">

      </div>

    <div class="contact-title the-how">
<!--      <h2 class="title">How will we do it?</h2>-->
<!--      <p>-->
<!--        Once you make the right choice and choose us to create your Mobile App. <br>-->
<!--        We will take the first step and do a Discovery Session, we will discuss your App, What are the goal of your app? <br>-->
<!--        what are the main features? What platform is right for your app? Who is your Audience? <br>-->
<!--        After this, we will assign a Product Manager to your app, that will be the point man <br>-->
<!--        in the whole process, he will send you weekly or biweekly reports on progress, he will<br>-->
<!--         supervise on all of the aspects of the app, and guide the UX designers,<br>-->
<!--         UI designers and developers through the whole process,<br>-->
<!--         and of course always be available for you.-->
<!--      </p>-->
    </div>

    <div class="contact-title the-process">
      <img class="triangleTop" src="<?php echo get_bloginfo('template_directory');?>/img/landing50k/tri-white-top.png" alt="">
      <div class="process-bubbles">
        <h2 class="title">Our Services</h2>
        <h3 class="subtitle">Get the benefits of working remote developers and still have a full-time Alpha team that is working, under one roof. </h3>
        <div class="container">

          <div class="row">

            <div class="col-xs-12 col-sm-6 col-md-3">
              <div class="iconColWrap">
                <i class="fa fa-filter"></i>
                <h3>Recruitment Services</h3>
                <p>We will recruit for you the best developers based on your needs and requirements. Bringing you a wide range of specialists from the IT industry.</p>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3">
              <div class="iconColWrap">
                <i class="fa fa-heart"></i>
                <h3>Simple Contract</h3>
                <p>Being the legal employer, we will be responsible for all of the administrative and legal aspects of the employment of the developers.</p>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3">
              <div class="iconColWrap">
                <i class="fa fa-users"></i>
                <h3>Dev Centers</h3>
                <p>All of your Alpha team will seat, side by side, in our dev centers across Europe. So you will have a centralized team that work efficiently and share knowledge. </p>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3">
              <div class="iconColWrap">
                <i class="fa fa-code"></i>
                <h3>Reduce Costs</h3>
                <p>Save time and money on recruitment, management and employment costs, We got you covered.</p>
              </div>
            </div>

            <div class="clear"></div>
          </div>
        </div>
      </div>
    </div>
	</section>









	<section id="contactUs" class="contact-us"   ng-controller="ContactController">
    <img class="triangleBottom" src="<?php echo get_bloginfo('template_directory');?>/img/landing50k/tri-white-bot.png" alt="">


    <div class="container">
			<div class="row">

				<div class="contact-title">
					<h2>Contact Us</h2>
				</div>

				<div class="col-sm-6">
					<div id="contact-form" class="form">
						<h3>Contact Us now to get the $50K App Offer</h3>


						<form name="userForm" novalidate>
							<input type="hidden" name="action" value="contact_send" />
							<div class="form-group" show-errors='{showSuccess: true}'>
								<label class="control-label"><?php _e('Name', 'grapps'); ?></label>
								<input tabindex="19" type="text" class="form-control" name="name" ng-model="user.name" required placeholder="<?php _e('Your name','grapps');?>" />
								<p tabindex="18" class="help-block ng-class:{ 'details-err-msg': userForm.name.$error.required }" ng-if="userForm.name.$error.required"><?php _e('Your name is required','grapps'); ?></p>
							</div>

							<div class="form-group heightFix" show-errors='{showSuccess: true}'>


								<label class="control-label"><?php _e('Subject','grapps'); ?></label>
								<?php if($grapps_lang == 'he') { ?>
								<select tabindex="24" name="subject" ng-model="user.subject" class="form-control input-subject" required  title="Subject" aria-label="Subject" ng-options="subject.subject as subject.subject for subject in subjectListHeb">
									<?php } else { ?>
									<select tabindex="24" name="subject" ng-model="user.subject" class="form-control input-subject" required  title="Subject" aria-label="Subject" ng-options="subject.subject as subject.subject for subject in subjectList">

										<?php } ?>
									</select>
							</div>




							<div class="form-group email-wrap input-field " show-errors='{showSuccess: true}'>
								<label class="control-label"><?php _e('Email','grapps'); ?></label>
								<input tabindex="28" type="email" class="form-control" name="email" ng-model="user.email" required placeholder="<?php _e('Your Email Address','grapps'); ?>" />

								<p tabindex="26" class="help-block ng-class:{ 'details-err-msg': userForm.email.$error.required }" ng-if="userForm.email.$error.required"><?php _e('Please enter your email','grapps');?></p>
								<p tabindex="27" class="help-block ng-class:{ 'details-err-msg': userForm.email.$error.email }" ng-if="userForm.email.$error.email"><?php _e('Something is wrong with this Email address','grapps');?></p>
							</div>




							<div class="form-group content-wrap input-field ">
								<label class="control-label"><?php _e('Content','grapps') ;?></label>
								<textarea tabindex="28" name="message" id="content" class="form-control" rows="5" ng-model="user.content"></textarea>
							</div>

							<div class="btn-contact-wrap">
								<span ng-show="isResponse" class="form-response-message" ng-class="responseError == true ? 'respose-fail' : '' ">{{user.thankyouorfailed}}</span>
							<span ng-show="isLoading" class="loading-spinner">
								<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
								<span class="sr-only">Loading...</span>
								</span>
								<button tabindex="29" ng-click="save()" type="submit" value="Send Message" class="btn btn-lg btn-success submit"  aria-label="Sendי" ><?php _e('Send','grapps'); ?></button>
							</div>
						</form>

					</div><!-- form -->
				</div><!-- col -->

				<div class="col-sm-6 preview-wrap">
					<h3 class="preview-title"><?php _e('See how we get your message','grapps'); ?></h3>

					<div class="preview">
						<h3><?php _e('Dear GRapps','grapps');?>,</h3>
						<h4>{{user.subject}}</h4>
						{{user.phoneNumber}}
						<div id="letter-content">
							{{user.content}}
						</div>
						<div class="letter-sig">
							<div><?php _e('Best Regards','grapps'); ?>,</div>
							<div ng-if="!user.name" class="placeholder-email"><?php _e('John Doe','grapps');?></div>
							<div class="">{{user.name}}</div>
							<div ng-if="!user.email" class="placeholder-email">info@grapps.io</div>
							<div class="">{{user.email}}</div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</section>






<section id="about2Container" class="section-80-130 whiteBgSection">

  <img class="triangleTop" src="<?php echo get_bloginfo('template_directory');?>/img/landing50k/tri-white-top.png" alt="">


  <div class="about-grapps">
    <h4 class="focus-test">FOCUS ON YOU</h4>
    <div class="container">
      <div class="row">
        <div class="col-sm-7 col-md-8">
          <div class="about-content about-content-bottom">
            <h2 class="title">About GRapps</h2>
            <p>GRapps is a Web apps & Mobile apps development company located in Israel. <br>
              We are an agile driven mobile apps dev company providing elite solutions & consulting for mobile markets. In the last few years, we started building dev teams for clients that have finished their product development and were in need for active maintenance, from this process grew the concept of “Alpha Teams”, building offshore dedicated teams of remote developers ,that physically sits in our dev center in Europe. We provide this service to help entrapanours, startups and enterprises to achieve their goals and move them towards their vision faster.
            </p>

          </div>
        </div>

        <div class="col-sm-5 col-md-4">
          <div class="about-typo">
            <span class="typo-tech">TECHNOLOGY</span>
            <span class="typo-focus">FOCUS ON YOU</span>
            <span class="typo-sidecode"><strong>GR</strong>APPS</span>
            <span class="typo-creative">CREATIVE</span>
            <span class="typo-bus">YOUR BUSINESS</span>

          </div>
        </div>

      </div>
    </div>
    <img class="triangleBottom" src="<?php echo get_bloginfo('template_directory');?>/img/landing50k/tri-white-bot.png" alt="">

  </div>


</section>



<section id="contactUs" class="contact-us" >
  <img class="triangleBottom" src="<?php echo get_bloginfo('template_directory');?>/img/landing50k/tri-white-bot.png" alt="">

</section>

  </div>









<script>
	// Scroll Smooth
	// handle links with @href started with '#' only
	$(document).on('click', 'a[href^="#"]', function(e) {
		// target element id
		var id = $(this).attr('href');

		// target element
		var $id = $(id);
		if ($id.length === 0) {
			return;
		}

		// prevent standard hash navigation (avoid blinking in IE)
		e.preventDefault();

		// top position relative to the document
		var pos = $(id).offset().top;

		// animated top scrolling
		$('body, html').animate({scrollTop: pos});
	});


</script>













  <!--Start of Tawk.to Script-->
  <script type="text/javascript">
  var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
  (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/578cb8faab0b003f24386a8e/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
  })();
</script>
<!--End of Tawk.to Script-->

<?php get_footer('landing50k'); ?>
