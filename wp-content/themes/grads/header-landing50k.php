<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<!-- Chrome, Firefox OS, Opera and Vivaldi -->
	<meta name="theme-color" content="#564462">
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#564462">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="#564462">
	<meta name="HandheldFriendly" content="True">
	<meta name="apple-mobile-web-app-capable" content="yes">

	<?php
	global  $grapps_lang;
	$grapps_lang= 'en';
	if (ICL_LANGUAGE_CODE == "he") {
		$grapps_lang = 'he';
	}
	?>


	<?php if (is_search()) { ?>
		<meta name="robots" content="noindex, nofollow" />

	<?php } ?>

	<title><?php wp_title(''); ?></title>


	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="/favicon.ico">
	<?php wp_head(); ?>

	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<?php if ($grapps_lang == 'he') { ?>
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/rtl.css" type="text/css" media="screen" />
	<?php } ?>
	<?php include (TEMPLATEPATH . '/inc/tracking_in_head.php' ); ?>
</head>

<body <?php body_class(); ?>>
<?php include (TEMPLATEPATH . '/inc/tracking_in_body.php' ); ?>



