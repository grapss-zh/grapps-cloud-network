
<?php
/**
 * Template Name: 404
 *
 * @package WordPress
 * @subpackage GRapps
 * @since GRapps 1.0
 */
?>


<?php get_header(); the_post(); ?>

<div class="top-border">

</div>
<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/style404.css" type="text/css" media="screen" />


<div class="page-404">
	<div class="wrappercont">
        <div id="logo-wrap">
            <a class="" href="<?php echo home_url(); ?>"><img src="<?php echo get_bloginfo('template_directory');?>/img/grapps.png"  alt="GRapps - Focus on you - Company Logo"/></a>
        </div>

		<h1 class="page-title"><?php _e('GRapps Ads Network','grapps'); ?></h1>
		<div class="content">
			<div class="title">404</div>
			<h2><?php _e('Page not Found');?></h2>
			<br>
			<h3><?php _e('Oops! what did you do??','grapps'); ?></h3>
			<br>

			<h4 class="guide-back"><a href="<?php echo home_url(); ?>"><?php _e('Let us take you home, click here.','grapps'); ?></a></h4>
		</div>
	</div>
</div>




<?php get_footer(); ?>
