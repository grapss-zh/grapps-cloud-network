<?php


require_once('inc/wp_bootstrap_navwalker.php');

add_action('after_setup_theme', 'grapps_theme_setup');
function grapps_theme_setup(){
	load_theme_textdomain('grapps', get_template_directory() . '/languages');
}
add_theme_support( 'title-tag' ); 

// ===============================================================
// ====== Register & enqueue latest jQuery ======================
// ===============================================================

function grapps_theme_enqueue_jquery() {
	if (!is_admin()) {
		wp_deregister_script('jquery');
		wp_register_script('jquery', 'https://code.jquery.com/jquery-2.1.4.min.js', false, '2.2.3');
		wp_enqueue_script('jquery');
	}
}
add_action('wp_enqueue_scripts', 'grapps_theme_enqueue_jquery');
// ===============================================================
// ============ Get Language =====================================
// ===============================================================

// ===============================================================
// ============ Register Sctipts =================================
// ===============================================================

// ===============================================================
// ============ Register Sctipts =================================
// ===============================================================



function grapps_theme_register_all_scripts_and_styles() {
	// ======== CSS
	wp_register_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', false, '1.1', 'all' );
	wp_register_style( 'owl', get_template_directory_uri() . '/css/owl.carousel.css', false, '1.1', 'all' );
	wp_register_style( 'animate', get_template_directory_uri() . '/css/animate.css', false, '1.1', 'all' );
	wp_register_style( 'landing50k', get_template_directory_uri() . '/css/landing50k.css', false, '1.1', 'all' );
  wp_register_style( 'landinggrteams', get_template_directory_uri() . '/css/landinggrteams.css', false, '1.1', 'all' );
	wp_register_style( 'lightcase', get_template_directory_uri() . '/css/lightcase.css', false, '1.1', 'all' );
	wp_register_style( 'slick', get_template_directory_uri() . '/css/slick.css', false, '1.1', 'all' );
	wp_register_style( 'offshoreHev1', get_template_directory_uri() . '/css/offshoreHev1.css', false, '1.1', 'all' );
	wp_register_style( 'offshoreHev1e2', get_template_directory_uri() . '/css/offshoreHev1e2.css', false, '1.1', 'all' );
	wp_register_style( 'offshoreHev1e3', get_template_directory_uri() . '/css/offshoreHev1e3.css', false, '1.1', 'all' );

	// ======== JS
	wp_register_script('bootstrap_js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), true);
	wp_register_script('isotope_js', get_template_directory_uri() . '/js/jquery.isotope.min.js', array('jquery'), true);
	wp_register_script('owl_js', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), true);
	wp_register_script('slick_js', get_template_directory_uri() . '/js/slick.min.js', array('jquery'), true);
	wp_register_script('lightcase_js', get_template_directory_uri() . '/js/lightcase.js', array('jquery'), true);

	wp_register_script('app_js', get_template_directory_uri() . '/js/app.js', array('jquery'), true);
    wp_register_script('landing50k_js', get_template_directory_uri() . '/js/landing50k.js', array('jquery'), true);
    wp_register_script('landinggrteams_js', get_template_directory_uri() . '/js/landinggrteams.js', array('jquery'), true);
	wp_register_script('offshoreHev1_js', get_template_directory_uri() . '/js/offshoreHev1.js', array('jquery'), true);


	// angular
	wp_register_script('angular_js', '//cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.8/angular.min.js', array('jquery'), true);

//	https:
//	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.js"></script>
//    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-aria.js"></script>


}
add_action('init', 'grapps_theme_register_all_scripts_and_styles');


add_theme_support( 'post-thumbnails' );


function grapps_theme_load_scripts_and_styles() {

	// ===== Global Scripts ==================
	// ===== CSS Files
	wp_enqueue_style('bootstrap');
	wp_enqueue_style('lightcase');

	wp_enqueue_style('slick');
	wp_enqueue_style('animate');

	// ===== JS Files
	wp_enqueue_script('bootstrap_js');
	wp_enqueue_script('app_js');
	wp_enqueue_script('lightcase_js');


	wp_enqueue_script('slick_js');
	wp_enqueue_script('slideout_js');
	wp_enqueue_script('jquerytouch_js');


	// ===== Conditional Scripts =============


	if(is_page_template('page-home.php')) {
		wp_enqueue_script('homepage_js');
	}
	if(is_page_template('page-about.php')) {
		wp_enqueue_script('about_js');
	}
	if(is_page_template('page-contact.php')) {
		wp_enqueue_script('angular_js');
		wp_enqueue_script('contact_js');
	}

	if(is_page_template('page-landing50k.php') || is_page_template('page-landing50kb.php')  ) {
		wp_enqueue_style('landing50k');
		wp_enqueue_script('angular_js');
		wp_enqueue_script('landing50k_js');
		wp_dequeue_script( 'app_js' );
	}

  if(is_page_template('page-landinggrteams.php')   ) {
    wp_enqueue_style('landinggrteams');
    wp_enqueue_script('angular_js');
    wp_enqueue_script('landinggrteams_js');
    wp_dequeue_script( 'app_js' );
  }


	if(is_page_template('page-offshore-he-v1.php') || is_page_template('page-offshore-he-v1e2.php') ||  is_page_template('page-offshore-he-v1e3.php')) {
		wp_enqueue_style('slick');
		wp_enqueue_style('landing50k');
		wp_enqueue_style('offshoreHev1');


		wp_enqueue_script('slick_js');
		wp_enqueue_script('angular_js');
		wp_enqueue_script('offshoreHev1_js');
		wp_dequeue_script( 'app_js' );

	}
	if(is_page_template('page-offshore-he-v1e2.php')) {
		wp_enqueue_style('offshoreHev1e2');
	}
	if(is_page_template('page-offshore-he-v1e3.php')) {
		wp_enqueue_style('offshoreHev1e3');
	}

}
add_action( 'wp_enqueue_scripts', 'grapps_theme_load_scripts_and_styles' );




//---------------------------------------------------------------------------
//  Send debug code to the Javascript console
//---------------------------------------------------------------------------

function debug_to_console($data) {
	if(is_array($data) || is_object($data))
	{
		echo("<script>console.log('PHP: ".json_encode($data)."');</script>");
	} else {
		echo("<script>console.log('PHP: ".$data."');</script>");
	}
}



// ===============================================================
// ====== ACF - Excerpt ======================================
// ===============================================================

register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'grapps' ),
	'footer_something'=> __('Footer Menu - Something Else','grapps')
) );




// =================================================================================
// ======================================= Ajax wp_mail() ==========================
// =================================================================================


add_action('wp', 'send_my_awesome_form');

function send_my_awesome_form(){

	if ( $_POST['form'] != "contactform") { return; }

	// get the info from the from the form
	$fullname = trim($_POST['fullname']);
	$company = trim($_POST['company']);
	$email = trim($_POST['email']);
	$honeypot = trim($_POST['address']);
	$currenturl = trim($_POST['currenturl']);
	$redirect = trim($_POST['redirect']);

	//Do all my required fields have a value?
	if ( $fullname == '' AND $email == '' AND $company == '' ) {
		$error = $currenturl.'?status=emptyvalues';
		wp_redirect($error); exit;
	}

	//Email header injection exploit prevention
	foreach ( $_POST as $value ){
		if (stripos($value, 'Content-Type:') != FALSE ) {
			$error = $currenturl.'?status=tryagain';
			wp_redirect($error); exit;
		}
	}

	//the spam honey pot
	if ($honeypot != '') {
		$error = $currenturl.'?status=tryagain';
		wp_redirect($error); exit;
	}

	//validate the email addess
	if ( !is_email( $email )) {
		$error = $currenturl.'?status=email';
		wp_redirect($error); exit;
	}


	// Build the message
	$message  = "Name :" . $fullname ."\n";
	$message .= "Company :" . $company  ."\n";
	$message .= "Email :" . $email     ."\n";

	//set the form headers
	$headers = 'From: Contact form <some@email.com>';

	// The email subject
	$subject = 'you got mail';

	// Who are we going to send this form too
	$send_to = 'your@email.com';


	if ( wp_mail( $send_to, $subject, $message, $headers ) ) {
		wp_redirect( $redirect ); exit;
	}

}

function awesome_form_errors() {

    if ( !$_SERVER['REQUEST_METHOD'] == 'GET') { return; }

    $status = $_GET['status'];

    if ($status == 'emptyvalues') {
			echo 'Please fill in all the fields and try again';
		}

		elseif ($status == 'tryagain') {
			echo 'Oops we had a problem formatting your form, please try again';
		}

		elseif ($status == 'email') {
			echo 'Please enter in a valid email address';
		}
}

//



// =================================================================================
// ====================Get Primary Category ========================================
// =================================================================================


function get_primary_cat_fields() {
	$category = get_the_category();
	if ( class_exists('WPSEO_Primary_Term') )
	{
		// Show the post's 'Primary' category, if this Yoast feature is available, & one is set
		$wpseo_primary_term = new WPSEO_Primary_Term( 'category', get_the_id() );
		$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
		$term = get_term( $wpseo_primary_term );
		if (is_wp_error($term)) {
			// Default to first category (not Yoast) if an error is returned
			$category_id =  $category[0]->term_id;
			$category_display = $category[0]->name;
			$category_link = get_category_link( $category[0]->term_id );
		} else {
			// Yoast Primary category
			$category_id =  $category[0]->term_id;
			$category_display = $term->name;
			$category_link = get_category_link( $term->term_id );
		}
	}
	else {
		// Default, display the first category in WP's list of assigned categories
		$category_id =  $category[0]->term_id;
		$category_display = $category[0]->name;
		$category_link = get_category_link( $category[0]->term_id );
	}
	if ($category_display == '') {
		$category_display = 'General';
	}


	return [
		'categoryLink' => $category_link,
		'categoryDisplayName' => $category_display,
	];
}

function limit_text($text, $limit) {
	if (str_word_count($text, 0) > $limit) {
		$words = str_word_count($text, 2);
		$pos = array_keys($words);
		$text = substr($text, 0, $pos[$limit]) . '...';
	}
	return $text;
}