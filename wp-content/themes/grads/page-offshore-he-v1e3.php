
<?php
/**
 * Template Name: Offshore he v1e3
 *
 * @package WordPress
 * @subpackage GRapps
 * @since GRapps 1.0
 */
?>

<?php get_header('offshore-he-v1'); ?>
<?php $grapps_lang = 'he'; ?>

<div id="ng-app" ng-app="offshore-he-v1" class="page-wrap grlanding50kb grlandingOffshoreV1E3">

    <section id="top-container">

        <a class="grapps-logo" href="#">
            <img tabindex="1" src="<?php echo get_bloginfo('template_directory');?>/img/grappswhite.png"  alt="GRapps - Focus on you - Company Logo" />
        </a>

        <div class="container-fluid">
            <div class="row top-row">

                <div class="col-sm-12 col-md-8 col-md-push-4 no-gutter">
                    <div class="sliderText">


                        <p class="ls-l ">
                            <br class="hidden-xs">
                            <span tabindex="3"  class="second-line">
                                איך להקים צוות פיתוח איכותי, לחסוך בזמן ועלויות, ולנהל אותו ביעילות עפ"י צרכי הפרויקט שלכם
                            </span>


                            <div class="slider-p-text">
                        <p tabindex="5" >
                            שוק הסטרטאפים הישראלי דוהר קדימה בקצב מסחרר, אלפי חברות נפתחות בארץ בכל שנה. אך לצד ההתפתחות המהירה והכסף הרב שמושקע בענף, בכל הקשור לכוח אדם - אנחנו פשוט  לא עומדים בקצב.
                        </p>
                        <p class="hidden-xs">
                            בישראל של היום קיים מחסור חמור של מהנדסים ומתכנתים. על-פי הערכות רשות החדשנות (לשעבר המדען הראשי) במשרד הכלכלה והתעשייה, המחסור יגיע ל-10,000 מהנדסים ומתכנתים בתוך כעשור בלבד!!! כבר היום אנחנו צופים במחסור של 4500 מפתחים בתעשייה.
                        </p>
                        <p><strong>
                            הפתרון שאנחנו מציעים ייחודי מאוד אך יחד עם זאת פשוט מאוד - גיוס צוות פיתוח משלכם במודל Offshore באוקראינה.
                            </strong></p>
                        <p class="hidden-xs4"><strong>
                            פתרון זה הוכח כיעיל עבור עשרות לקוחותינו, חברות סטרטאפ מהמובילות בתעשייה, אשר ישמחו לספר לכם כיצד אנו עוזרים להם, באמצעות כלים מתקדמים ובניית מאגר מידע איכותי, להקים צוות פיתוח ייחודי עבורם.
                        </strong></p>
                        <p>
                            ההבטחות שלנו אליכם הן פשוטות:
                        </p>

                        <ol>
                            <li>הפרויקטים שלכם יפוקחו וינוהלו בסטנדרט גבוה כך שתצליחו להפיק יותר ערך מהכסף שלכם.</li>
                            <li>
                                לרוב ההוצאות המשמעותיות ביותר הם עבור כוח אדם, איתנו הוצאה זו תצטמצם ב 50%, ובמספרים: עלות שכר של מתכנת ממוצע, עם 3 שנים ניסיון, היא כ-2,500 דולר.
                            </li>
                            <li>
                                נקבל מכם דרישות משרה ותוך שבועיים נקבע לכם ראיון ראשון עם מועמדים רלוונטיים לאישורכם, תהליך זה הינו ללא עלות.
                            </li>
                        </ol>
                        </div>

                        <div class="button-wrap">
                            <a tabindex="6" href="#aboutContainer" class="buttonBig">למד עוד</a>
                            <a tabindex="7" href="#contactUs" class="buttonBig">צור קשר</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-4 col-md-pull-8 top-image-wrap">
                    <img class="top-img-slider" src="<?php echo get_bloginfo('template_directory');?>/img/offshore-he/paper2.png" alt="Phone" class="ls-l ls-preloaded">
                </div>
        </div>


        </div>
    </section>



<section id="aboutContainer" class="section-80-130 whiteBgSection">

    <img class="triangleTop" src="<?php echo get_bloginfo('template_directory');?>/img/offshore-he/tri-white-top.png" alt="">


    <div class="about-grapps">
        <h4 class="focus-test">FOCUS ON YOU</h4>
        <div class="container">
            <div class="row">

                <div class="col-sm-6 col-md-7 col-sm-push-6 col-md-push-5">
                    <div class="about-content">
                        <h2 class="title" tabindex="16">איך מתחילים לעבוד איתנו ?</h2>

                        <ul>
                            <ol>
                                <li><strong>ניפגש אצלכם / אצלנו </strong>או שנבצע שיחה טלפונית בכדי ללמוד ולהבין את הדרישות ואופי החברה.</li>
                                <li><strong> תחקור ותאום צפיות:</strong> אתם מעבירים דרישות לתפקיד, אנחנו ננתח יחד עם ה CTO שלנו את הדרישות כך נבנה פרופיל משרה מדויק ואיכותי עבורכם. </li>
                                <li>בשלב זה כבר הבנתם את הייחודיות שלנו ואנו <strong>מתחילים בתהליך גיוס ללא עלות</strong> (הדבר היחידי שאנו מבקשים מכם הוא זמינות לתהליך הגיוס שאמור לקחת בין שבוע לחודש).</li>
                                <li>כאן אנשי כוח האדם שלנו נכנסים לפעולה <strong>ובוררים את קו"ח האיכותיים ביותר</strong>, מבצעים ראיון עם מנהלת כוח אדם, מבחן מקצועי, ראיון עם CTO שלנו ובסופו של תהליך <strong>ראיון משותף אתכם</strong>.</li>
                                <li>תיאום צפיות, חתימה על חוזה סטנדרטי <strong>ותחילת העבודה</strong>, הבידול שלנו יתבטא בעבודה אתכם לטווח הארוך!</li>
                            </ol>
                        </ul>


                    </div>
                </div>



                <div class="col-sm-6 col-md-5 col-sm-pull-6 col-md-pull-7">
                    <div class="contactUs2"  ng-controller="Contact2Controller">
                        <form name="userForm" novalidate>
                            <h2 class="get-offer-now-title">השאירו פרטים כאן ואחד מנציגינו יחזרו אליכם בהקדם!</h2>
                            <input type="hidden" name="action" value="contact_send" />
                            <div class="form-group" show-errors='{showSuccess: true}'>
                                <label class="control-label">* <?php _e('שם', 'grapps'); ?></label>
                                <input tabindex="9" type="text" class="form-control" name="name" ng-model="user.name" required placeholder="<?php _e('מה השם שלך?','grapps');?>" />
                                <p tabindex="8" class="help-block ng-class:{ 'details-err-msg': userForm.name.$error.required }" ng-if="userForm.name.$error.required"><?php _e('אנא הכנס את שמך','grapps'); ?></p>
                            </div>




                            <div class="form-group email-wrap input-field " show-errors='{showSuccess: true}'>
                                <label class="control-label">* <?php _e('אימייל','grapps'); ?></label>
                                <input tabindex="12" type="email" class="form-control" name="email" ng-model="user.email" required placeholder="<?php _e('כתובת המייל שלך','grapps'); ?>" />

                                <p tabindex="11" class="help-block ng-class:{ 'details-err-msg': userForm.email.$error.required }" ng-if="userForm.email.$error.required"><?php _e('אנא הכנס כתובת מייל','grapps');?></p>
                                <p tabindex="10" class="help-block ng-class:{ 'details-err-msg': userForm.email.$error.email }" ng-if="userForm.email.$error.email"><?php _e('כתובת מייל לא תקינה','grapps');?></p>
                            </div>




                            <div class="form-group phone-wrap input-field " show-errors='{showSuccess: true}'>
                                <label class="control-label">טלפון</label>
                                <input tabindex="13" type="phone" class="form-control" name="phone" ng-model="user.phone" required placeholder="<?php _e('מספר הטלפון שלך','grapps'); ?>" />

                            </div>





                            <div class="btn-contact-wrap">
                                <span ng-show="isResponse" class="form-response-message" ng-class="responseError == true ? 'respose-fail' : '' ">{{user.thankyouorfailed}}</span>
                                <span ng-show="isLoading" class="loading-spinner">
                  <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                  <span class="sr-only">Loading...</span>
                  </span>
                                <button tabindex="14" ng-click="save()" type="submit" value="Send Message" class="btn btn-lg btn-success submit"  aria-label="שלח" ><?php _e('שלח','grapps'); ?></button>
                            </div>
                        </form>
                    </div>
                </div>


            </div>
        </div>
        <img class="triangleBottom" src="<?php echo get_bloginfo('template_directory');?>/img/offshore-he/tri-white-bot.png" alt="">

    </div>


    <section class="contact-title the-how">
        <div class="the-how-wrap">
            <h2 class="title">הסיפור שלנו</h2>
            <p>
                לפני קצת יותר משלוש שנים, כשרק התחלנו את דרכינו העצמאית והוצאנו לאוויר העולם לראשונה את GRapps - חברה טכנולוגית המתמחה באפיון ובפיתוח אפליקציות, חווינו על בשרינו את הקושי העצום הכרוך בגיוס אנשי צוות איכותיים לחברה, כמות המשאבים (בעיקר זמן וכסף) שהשקענו בתחילת דרכינו בשביל לגייס מפתח איכותי לשורותינו היו עצומים, אבל חמור מכך, לעיתים אף נאלצנו להתפשר בדרישות ולהוריד סטנדרטים נוכח ההיצע הנמוך ומורכבות התהליך.            </p>
            <p>
                אנחנו משוכנעים שאתם יודעים זאת טוב בדיוק כמונו, תהליך הגיוס הצריך מאתנו להשקיע זמן וכסף בפרסום המשרה עבור מועמדים רלוונטיים (במקרים מסוימים לקח לנו כ 3-4 חודשים עד שהגיע מועמד באמת רלוונטי), להשקיע זמן במעבר על קורות חיים ולאחר מכן בראיונות טלפונים, ראיונות פרונטליים, עריכת מבחנים מקצועיים ובדיקתם, ראיונות נוספים למועמדים המובילים ואז החלק ה״כייפי״, משא ומתן על דרישות השכר... וכך רק במידה והכל הלך חלק, תוך 3-6 חודשים הצלחנו לגייס את הטאלנט שלנו!
            </p>
            <p class="underline">
                אז זה הסיפור שלנו אבל אל דאגה, סדרנו לכם ולנו סוף טוב.
            </p>
            <p>
                כמו בכל תחום בחיינו כיזמים, לא הסכמנו לקבל את המציאות כפי שהיא, הבנו שחייב להיות פתרון שנותן מענה לעניין והבנו שברגע שנמצא את אותו פתרון, נבנה את המודל הנכון והמדויק שיאפשר לנו להציע את אותו מודל בדיוק לכל אותן חברות שחוו בעיה זהה לשנו.
            </p>
            <p>
                וכך, לפני כמעט שנתיים ולאחר מחקר שוק מטורף, הקמנו סניף ראשון של GRapps באוקראינה!
            </p>
            <p>
                היום אנחנו גאים להעסיק כ 30 מפתחים ומהנדסים בשני סניפים באוקראינה, אחד בזי׳טומיר והשני בקייב אשר משרתים עשרות חברות סטארטאפ ישראליות במודל שנקרא על פי רוב Offshore service.
            </p>
            <p>
                רק שאנחנו ב GRapps לקחנו את מודל ה Offshore service ה"קלאסי" ושדרגנו אותו בכמה רמות, למען האמת לפחות פי 10.
            </p>
            <p>
                איך שדרגנו אותו? כל התשובות מופיעות בהמשך.
            </p>
        </div>








	    <?php
	    $images = get_field('our_story_gallery_slider');
	    if( $images ) { ?>
            <div class="slick-office-wrap" style="direction: ltr">
                <div id="slick-office">
	                <?php foreach( $images as $image ) { ?>
                        <div class="item">
                            <a href="<?php echo $image['url']; ?>" data-rel="lightcase:office">
                                <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>">
                            </a>
                        </div>
	                <?php } ?>
	                <?php foreach( $images as $image ) { ?>
                        <div class="item">
                            <a href="<?php echo $image['url']; ?>" data-rel="lightcase:office">
                                <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>">
                            </a>
                        </div>
	                <?php } ?>
                </div>
            </div>
	    <?php } ?>



   </section>


    <section class="contact-title the-process">
        <img class="triangleTop" src="<?php echo get_bloginfo('template_directory');?>/img/offshore-he/tri-white-top.png" alt="">
        <div class="process-bubbles">
            <h2 class="title">אז איך הקמת המשרדים שלנו באוקראינה,<br class="hidden-xs"> והידע הרב בניהול הפרויקט שצברנו עם השנים,<br class="hidden-xs"> יפתרו לכם את בעיית הגיוס?</h2>
            <h3 class="subtitle">באף מקום בעולם לא קל למצוא אנשי מקצוע מעולים, אך כמו ברוב המקרים, הניסיון משחק תפקיד מרכזי. ההיכרות שלנו את השוק המקומי, ה DB האיכותי שצברנו, המתודולוגיות שפתחנו ויישמנו והידע שצברנו עם השנים, מאפשרים לנו לספק ללקוחות שלנו גישה למהנדסים ולמפתחים מעולים בעלויות הרבה יותר נמוכות מהשוק הישראלי.
            </h3>
            <div class="container">

                <div class="row">


                    <div class="col-sm-12 col-md-6">
                        <div class="iconColWrap">
                            <i class="fa fa-clock-o"></i>
                            <h3>חיי משרד</h3>
                            <p>
                                דמיינו לכם שהעובדים שלכם נהנים מ <strong>Happy Hour</strong> כל שבוע, שיעורי העשרה, מסיבות חברה ועוד מגוון פעילויות חבריות, נדמה לכם שזה בשדרות רוטשליד ? לא, כך חיים העובדים שלכם אצלינו במשרד, אנחנו דואגים להתנהלות חברתית של אנשי הפיתוח כך שיגיעו למשרד בכל יום עם <strong>חיוך ומוטיבציה.</strong>
                            </p>
                            <p>
                                העובדים פעילים 5 ימים בשבוע בין 9:00 ל 18:00 כאשר כל יום נפתח ב <strong>StandUp</strong>, תהליך בו כל אנשי המשרד מתכנסים למעגל בהנחיית מנהל המשרד וכל מפתח תוכנה ומנהל פרויקטים מספר שם על המשימה היומית שלו וכן על <strong>האתגרים</strong>, כל המידע בכל יום מדווח באקסלים למנהלים (בשעה 18:00 מתקיים StandUp סגירת יום ובחינת היעדים).
                            </p>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6">
                        <div class="iconColWrap">
                            <i class="fa fa-file-text"></i>
                            <h3>התנהלות פרויקט</h3>
                            <p>
                            </p>
                            <p>רמה ניהולית: העבודה איתנו מתחילה רק אחרי שבחרנו בפינצטה <strong>מפתח מתאים</strong> עבורכם, כשאתם עובדים איתנו אתם מקבלים שכבות ניהול לכל פרוייקט בו אנו מעורבים ע"מ לוודא הצלחה. בנוסף למפתח מצוות לכל פרוייקט <strong>מנהל פרוייקטים</strong> דובר אנגלית שוטפת ששולט בכל משימה ומשימה, מתעדף ומתזמן משימות בהתאם ליעדי המיזם והוא האיש שלכם שדואג לספק <strong>תמונת מצב</strong> עדכנית.</strong></p>
                            <p>במשרד, <strong>Team leader</strong> מקצועי (טכנולוג) ומנוסה שתפקידו ללוות את אנשי הפיתוח ולהנחות אותם בצמתים מרכזיות, בנוסף, <strong> CTO ישראלי</strong>  (אחד הבעלים בחברה) שמלווה בייעוץ והכוונה כל פרוייקט ולפעמים אף נכנס לקוד.
                            <p>
                                בנוסף אנו נותנים גישה לייעוץ מומחים בתחומים של UX, שיווק המוצר וניהול המוצר כחלק מהמעטפת של העבודה איתנו.
                            </p>
                            <p>
                                    אז לסיכום: כשאתם משלמים על פעילות מפתח אתם מקבלים בנוסף <strong>מעטפת שלמה</strong> באותו המחיר!
                                </p>
                        </div>
                    </div>












                    <div class="col-sm-12 col-md-6">
                        <div class="iconColWrap">
                            <i class="fa fa-leaf"></i>
                            <h3>שקט תעשייתי</h3>
                            <p>
                                שקט תעשייתי: הלקוחות שלנו מספרים שעבורם היתרונות המשמעותיים ביותר בעבודה מול <strong>צוות פיתוח</strong> באוקראינה באים לידיי ביטוי בחריצות, <strong>מוסר עבודה גבוה</strong>, משמעת גבוהה בנוגע ל<strong>עמידה ביעדים</strong> ולוחות זמנים, יכולת למלא תפקיד כראוי וזאת מבלי החיכוכים שבניהול רגיל, כל אלו מייצרים עבור הלקוחות <strong>איזון מושלם</strong> למיזם שלהם.
                            </p>
                        </div>
                    </div>



                    <div class="col-sm-12 col-md-6">
                        <div class="iconColWrap">
                            <i class="fa fa-heart"></i>
                            <h3>לב ישראלי</h3>
                            <p>
                                למרות שמרכז הפיתוח של החברה פועל מחוץ לגבולותיה של מדינת ישראל, חשוב לנו מאוד <strong>לדאוג לצרכי העובדים</strong> וזכויותיהם ואף הרבה יותר מכך, לכן, כל העובדי החברה עובדים תחת חוקי המדינה ותחת פיקוח וניהול של עו"ד ורו"ח גם בישראל וגם באוקראינה. באותה נשימה נציין כי כל עובד ועובדת, גויסו לחברה בקפידה רבה ולכן <strong>חשובים לנו ברמה האישית</strong>, ואנו מצפים ודואגים שיקבלו <strong>יחס ראוי</strong> כאנשי מקצוע.
                            </p>
                            <p>
                                המשרד הראשי בישראל נמצא בבנימינה (דקה הליכה מתחנת הרכבת) ואתם <strong>תמיד מוזמנים לקפה</strong>.

                            </p>
                        </div>
                    </div>





                    <div class="col-sm-12 col-md-6">
                        <div class="iconColWrap">
                            <i class="fa fa-users"></i>
                            <h3>הכי ישראלי</h3>
                            <p>
                                חשוב לציין שהחברה באוקראינה היא <strong>ישראלית</strong>, כלומר: החברה באוקראינה בבעלות <strong>GRapps</strong> <strong>ובניהול ישראלי מלא</strong>, השולחנות, המחשבים, הקפה וכל שאר הפסילטיז.
                            </p>
                            <p>
                                תרצו שהניהול יהיה בעברית מולכם ? נוכל לצוות מנהל מוצר ישראלי.
                            </p>
                        </div>
                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="iconColWrap">
                            <i class="fa fa-filter"></i>
                            <h3>ניהול פרויקט טכנולוגי</h3>
                            <p>
                                אנו עובדים עפ"י <strong>הסטנדרטים הגבוהים</strong> ביותר, ולכן אנו מתאימים את שיטות הניהול ל<strong>צרכי העסק שלכם</strong>, כל פרוייקט מנוהל במטודולודיית <strong>Agile Development</strong>  במערכת <strong>JIRA</strong> או בכל שיטה וכלי ניהולי אליהם אתם רגילים וכל זאת נועד בשביל לגשר על פערי המרחק הגיאוגרפים ולייצר <strong>תחושה שהמפתח יושב אתכם</strong> ממש באותו החדר.
                            </p>
                        </div>
                    </div>



                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>
</section>









<section id="contactUs" class="contact-us"   ng-controller="ContactController">
    <img class="triangleBottom" src="<?php echo get_bloginfo('template_directory');?>/img/offshore-he/tri-white-bot.png" alt="">


    <div class="container">
        <div class="row">

            <h2 class="contact-title">
                <h2 class="main-title">הצטרפו לחברות שכבר עובדות איתנו</h2>
                <h3 style="text-align: center" class="subtitle">ותשמעו מה שיש להם להגיד</h3>

	            <?php
	            $images = get_field('clients_gallery_slider');
	            if( $images ) { ?>
                    <div class="slick-clients-wrap" style="direction: ltr">
                        <div id="slick-clients">
				            <?php foreach( $images as $image ) { ?>
                                <div class="item">
                                        <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>">
                                </div>
				            <?php } ?>
				            <?php foreach( $images as $image ) { ?>
                                <div class="item">
                                    <a href="<?php echo $image['url']; ?>">
                                        <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>">
                                    </a>
                                </div>
				            <?php } ?>
                        </div>
                    </div>
	            <?php } ?>


                <h2 class="main-title-2">הצטרפו עכשיו</h2>
            </div>


            <div class="col-sm-6 col-sm-push-6">
                <div id="contact-form" class="form">
                    <h3>השאירו פרטים כאן ואחד מנציגינו יחזרו אליכם בהקדם</h3>


                    <form name="userForm" novalidate>
                        <input type="hidden" name="action" value="contact_send" />
                        <div class="form-group" show-errors='{showSuccess: true}'>
                            <label class="control-label"><?php _e('שם', 'grapps'); ?></label>
                            <input tabindex="19" type="text" class="form-control" name="name" ng-model="user.name" required placeholder="<?php _e('השם שלך','grapps');?>" />
                            <p tabindex="18" class="help-block ng-class:{ 'details-err-msg': userForm.name.$error.required }" ng-if="userForm.name.$error.required"><?php _e('Your name is required','grapps'); ?></p>
                        </div>

                        <div class="form-group heightFix" show-errors='{showSuccess: true}'>


                            <label class="control-label"><?php _e('נושא','grapps'); ?></label>
					        <?php if($grapps_lang == 'he') { ?>
                            <select tabindex="24" name="subject" ng-model="user.subject" class="form-control input-subject" required  title="Subject" aria-label="Subject" ng-options="subject.subject as subject.subject for subject in subjectListHeb">
						        <?php } else { ?>
                                <select tabindex="24" name="subject" ng-model="user.subject" class="form-control input-subject" required  title="Subject" aria-label="Subject" ng-options="subject.subject as subject.subject for subject in subjectList">

							        <?php } ?>
                                </select>
                        </div>




                        <div class="form-group email-wrap input-field " show-errors='{showSuccess: true}'>
                            <label class="control-label"><?php _e('אימייל','grapps'); ?></label>
                            <input tabindex="28" type="email" class="form-control" name="email" ng-model="user.email" required placeholder="<?php _e('כתובת המייל שלך','grapps'); ?>" />

                            <p tabindex="26" class="help-block ng-class:{ 'details-err-msg': userForm.email.$error.required }" ng-if="userForm.email.$error.required"><?php _e('אנא הכנס כתובת מייל','grapps');?></p>
                            <p tabindex="27" class="help-block ng-class:{ 'details-err-msg': userForm.email.$error.email }" ng-if="userForm.email.$error.email"><?php _e('כתובת מייל לא תקינה','grapps');?></p>
                        </div>




                        <div class="form-group content-wrap input-field ">
                            <label class="control-label"><?php _e('הודעה','grapps') ;?></label>
                            <textarea tabindex="28" name="message" id="content" class="form-control" rows="5" ng-model="user.content"></textarea>
                        </div>

                        <div class="btn-contact-wrap">
                            <span ng-show="isResponse" class="form-response-message" ng-class="responseError == true ? 'respose-fail' : '' ">{{user.thankyouorfailed}}</span>
                            <span ng-show="isLoading" class="loading-spinner">
								<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
								<span class="sr-only">טוען...</span>
								</span>
                            <button tabindex="29" ng-click="save()" type="submit" value="Send Message" class="btn btn-lg btn-success submit"  aria-label="Sendי" ><?php _e('שלח','grapps'); ?></button>
                        </div>
                    </form>

                </div><!-- form -->
            </div><!-- col -->

            <div class="col-sm-6 col-sm-pull-6 preview-wrap">
                <h2 class="preview-title"><?php _e('ראה כיצד ההודעה שלך נראית בדרך אלינו','grapps'); ?></h2>

                <div class="preview">
                    <h3><?php _e('Dear GRapps','grapps');?>,</h3>
                    <h4>{{user.subject}}</h4>
                    {{user.phoneNumber}}
                    <div id="letter-content">
                        {{user.content}}
                    </div>
                    <div class="letter-sig">
                        <div><?php _e('תודה','grapps'); ?>,</div>
                        <div ng-if="!user.name" class="placeholder-email"><?php _e('ישראל ישראלי','grapps');?></div>
                        <div class="">{{user.name}}</div>
                        <div ng-if="!user.email" class="placeholder-email">info@grapps.io</div>
                        <div class="">{{user.email}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>






<section id="about2Container" class="section-80-130 whiteBgSection">

    <img class="triangleTop" src="<?php echo get_bloginfo('template_directory');?>/img/offshore-he/tri-white-top.png" alt="">


    <div class="about-grapps">
        <h4 class="focus-test-2">FOCUS ON YOU</h4>
        <div class="container">
            <div class="row">




                <div class="col-sm-7 col-sm-push-5 col-md-8 col-md-push-4">
                    <div class="about-content">
                        <h2 class="title">אודות GRapps</h2>
                        <p>
                            Grapps היא חברת פיתוח אפליקציות מובייל ופיתוח אתרים, נוסדה ב-2013.
                            אנחנו מנוסים בפיתוח אפליקציות מובייל ובניית אתרים עם אהבה גדולה למטודולוגיית Lean Startup.
                            הפילוסופיה של GRapps בנוגע לשירות לקוחות היא לא רק לבצע עבודה איכותית במחיר הוגן, זה הסטנדרט שלנו. שירות לקוחות מעולה מבחינתנו מתבטא בללוות את הלקוח ולהדריך אותו בכל שלב בדרך, להבין את הצרכים שלו ולגרום לו להשיג את המטרות שהציב לעצמו, אבל הכי חשוב מבחינתנו היא להפוך את התהליך עבור הלקוח לתהליך נעים וכיפי שיגרום לו לחייך לאורך כל הדרך.
                            כחברת פיתוח אפליקציות מנוסה שמשרתת יזמים בתהליך פיתוח המוצר, סטארטפים, חברות מדיה וחברות גדולות בתחומים שונים בתעשייה, אנחנו מוודאים שצוות הפיתוח יכסה כל צורך אפשרי ויעזור ללקוח להגיע לתוצאות משמעותיות.
                        </p>

                    </div>
                </div>

                <div class="col-sm-5 col-sm-pull-7 col-md-4 col-md-pull-8">
                    <div class="about-typo">
                        <span class="typo-tech">TECHNOLOGY</span>
                        <span class="typo-focus">FOCUS ON YOU</span>
                        <span class="typo-sidecode"><strong>GR</strong>APPS</span>
                        <span class="typo-creative">CREATIVE</span>
                        <span class="typo-bus">YOUR BUSINESS</span>

                    </div>
                </div>



            </div>
        </div>
        <img class="triangleBottom" src="<?php echo get_bloginfo('template_directory');?>/img/offshore-he/tri-white-bot.png" alt="">

    </div>


</section>



<section id="contactUsFooter" class="contact-us contact-us-footer" >
    <img class="triangleBottom hidden-xs" src="<?php echo get_bloginfo('template_directory');?>/img/offshore-he/tri-white-bot.png" alt="">
</section>







<?php get_footer('offshore-he-v1'); ?>
